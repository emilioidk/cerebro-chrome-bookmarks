// TODO: Watch for Bookmarks files changes and update on changes
// TODO: Allow setting for # of results
// TODO: Allow setting for selecting search fields
import icon from '../assets/icon.png'

import FuzzySearch from 'fuzzy-search';

import { homedir } from 'os';
import path from 'path';
import fs from 'fs';

const googleChromeHomeDir = path.resolve(homedir(), '.config', 'google-chrome');
let googleChromeProfiles = ['Default'];

const googleChromeHomeDirContents = fs.readdirSync(googleChromeHomeDir);
const profiles = googleChromeHomeDirContents.filter(x => x.startsWith('Profile'));

googleChromeProfiles = googleChromeProfiles.concat(profiles);

export const name = 'Google Chrome Bookmarks';
export const keyword = 'bm';

let bookmarks = [];
const bookmarksProfiles = {};

function transverseFolder(profile, folder) {
  for (let item of folder.children) {
    if (item.type === 'url') {
      bookmarks.push({ name: item.name, url: item.url, profile });
    } else if (item.type === 'folder') {
      transverseFolder(profile, item);
    }
  }
}

function ensureBookmarks(profile, isLoaded = false) {
  if (!isLoaded) {
    if (bookmarksProfiles[profile] === undefined) {
      return;
    }
    delete bookmarksProfiles[profile];
    bookmarks = bookmarks.filter(bookmark => bookmark.profile !== profile);
  } else {
    if (bookmarksProfiles[profile]) {
      return;
    }
    bookmarksProfiles[profile] = true;
    const bookmarkFilePath = `${googleChromeHomeDir}/${profile}/Bookmarks`;
    const fileContent = fs.readFileSync(bookmarkFilePath, { encoding: 'utf-8' });
    try {
      const parsedFile = JSON.parse(fileContent);
      const rootsFolders = Object.keys(parsedFile.roots);
      for (let folder of rootsFolders) {
        if (parsedFile.roots[folder].type === 'folder') {
          transverseFolder(profile, parsedFile.roots[folder]);
        }
      }
    } catch (error) {
      // console.log(error)
      //moving on...
    }
  }
}

export const fn = ({ actions, term, display, settings }) => {
  let searchTerm = term;
  if (!settings.skipprefix) {
    if (!term.startsWith('bm ')) {
      return;
    }
    searchTerm = term.substr(3);
  }
  googleChromeProfiles.forEach(profile => ensureBookmarks(profile, settings[profile]))

  const searcher = new FuzzySearch(bookmarks, ['name', 'url'], {
    caseSensitive: false,
    sort: true
  });

  const results = searcher.search(searchTerm);

  display(results.map(item => {
    return {
      title: item.name,
      subtitle: item.url,
      icon,
      clipboard: item.url,
      onSelect: () => actions.open(item.url)
    }
  }))
}

const profilesOptions = {};

for (const profile of googleChromeProfiles) {
  profilesOptions[profile] = {
    label: profile,
    description: `Include items from ${googleChromeHomeDir}/${profile}/Bookmarks in results`,
    type: 'bool',
  }
}

export const settings = {
  skipprefix: {
    label: 'Skip "bm" prefix',
    description: 'Search from bookmarks without prefixing the search with "bm".',
    type: 'bool'
  },
  ...profilesOptions
}
