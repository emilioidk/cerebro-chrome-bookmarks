# Cerebro Chrome Bookmarks

Search through your Google Chrome bookmarks.

![](assets/demo.gif)

## Usage

Start your input with `bm ` and then add the string you would like to search. Settings for the plugin
allow configuring whether the `bm` prefix should be ignored and will allow you to select which Google
Chrome Profiles should be considered. The keyboard shortcut `CONTROL+C` will copy the bookmark URL to the
clipboard, while selecting the item on the UI will open the bookmark in Google Chrome

## NOTE
This plugin will has been only been tested in Linux and it is unlikely to work in other operating systems.
